#!/usr/bin/env python

map_words_to_number = {
    "one" : "1",
    "two" : "2",
    "three" : "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9"
}

def main():
    solution_part1 = 0
    solution_part2 = 0
    with open('input.txt', encoding="utf-8") as f:
        for line in f:
            min_part2 = -1
            max_part2 = -1
            min_part1 = -1
            max_part1 = -1
            digit_pos_list = []
            for char in enumerate(line.strip()):
                if char[1].isdigit():
                    digit_pos_list.append(char[0])
            if len(digit_pos_list) > 0:
                min_part1, max_part1 = line[digit_pos_list[0]], line[digit_pos_list[-1]]
                full_number_part1 = min_part1 + max_part1
                solution_part1 = solution_part1+int(full_number_part1)
            #part2
            left = []
            right = []
            for word in map_words_to_number:
                if line.strip().find(word) > -1:
                    left.append((line.strip().find(word), word))
                if line.strip().rfind(word) > -1:
                    right.append((line.strip().rfind(word), word))
            if len(left) > 0:
                if int(min_part1) > -1:
                    if min(left)[0] < int(line.find(min_part1)):
                        min_part2 = map_words_to_number[min(left)[1]]
                    else:
                        min_part2 = min_part1
                else:
                    min_part2 = map_words_to_number[min(left)[1]]
            else:
                min_part2 = min_part1
            if len(right) > 0:
                if int(max_part1) > -1:
                    if max(right)[0] > int(line.rfind(max_part1)):
                        max_part2 = map_words_to_number[max(right)[1]]
                    else:
                        max_part2 = max_part1
                else:
                    max_part2 = map_words_to_number[max(right)[1]]
            else:
                max_part2 = max_part1
            full_number_part2 = str(min_part2) + str(max_part2)
            solution_part2 = solution_part2+int(full_number_part2)
    print(f"solution part1: {solution_part1}")
    print(f"solution part2: {solution_part2}")

if __name__ == '__main__':
    main()
