#!/usr/bin/env python

numbers_of_wins_per_card = {}
total_number_of_cards = {}

def main():
    with open("input.txt", encoding="utf-8") as f:
        solution_part1 = 0
        solution_part2 = 0
        card_counter = 1
        for line in f:
            winning_numbers = list(line.strip().split(":")[1].split("|")[0].strip().split())
            numbers_on_card = list(line.strip().split(":")[1].split("|")[1].strip().split())
            matches = set(numbers_on_card) & set(winning_numbers)
            card_points = 0
            if len(matches) == 1:
                card_points = 1
            for i in range(len(matches)-1):
                if i == 0:
                    card_points = 1
                card_points = card_points + card_points
            total_number_of_cards[card_counter] = 1
            numbers_of_wins_per_card[card_counter] = len(matches)
            solution_part1 = solution_part1 + card_points
            card_counter += 1
        #part2
        for card_counter in range(1,len(numbers_of_wins_per_card)+1):
            wins = numbers_of_wins_per_card[card_counter]
            if wins > len(numbers_of_wins_per_card)-card_counter:
                wins = len(numbers_of_wins_per_card)-card_counter
            for i in range(1,wins+1):
                total_number_of_cards[card_counter+i] = total_number_of_cards[card_counter+i] + 1 * total_number_of_cards[card_counter]
        for _,value in total_number_of_cards.items():
            solution_part2 = solution_part2 + value
        print(f"solution part1: {solution_part1}")
        print(f"solution part2: {solution_part2}")

if __name__ == '__main__':
    main()
