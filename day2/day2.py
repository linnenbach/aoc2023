#!/usr/bin/env python

number_dices = {
    "red" : 12,
    "green": 13,
    "blue": 14
}

def main():
    solution_part1 = 0
    solution_part2 = 0
    with open('input.txt', encoding="utf-8") as f:
        for line in f:
            game_id = line.strip().split(":")[0].split(" ")[1]
            game_rounds = line.strip().split(":")[1].split(";")
            game_possible = True
            #part2 var
            min_number_dices = {
                "red": 0,
                "green": 0,
                "blue": 0
            }
            for game in game_rounds:
                dices_per_round = game.split(",")
                for dices in dices_per_round:
                    number_dices_round, color_dices = dices.strip().split(" ")
                    if number_dices[color_dices] < int(number_dices_round):
                        game_possible = False
                    if int(number_dices_round) > min_number_dices[color_dices]:
                        min_number_dices[color_dices] = int(number_dices_round)
            if game_possible:
                solution_part1 = solution_part1 + int(game_id)
            part2_round_solution = min_number_dices["red"] * min_number_dices["green"] * min_number_dices["blue"]
            solution_part2 = solution_part2 + part2_round_solution
    print(f"{solution_part1}")
    print(f"{solution_part2}")

if __name__ == '__main__':
    main()
